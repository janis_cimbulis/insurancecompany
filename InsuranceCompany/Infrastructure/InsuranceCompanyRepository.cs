using System;
using System.Collections.Generic;
using System.Linq;
using Insurance.Interfaces;
using Insurance.Models;
using Insurance.Structs;

namespace Insurance.Infrastructure
{
    public class InsuranceCompanyRepository : IInsuranceCompanyRepository
    {
        private IPolicyContext ctx;

        public InsuranceCompanyRepository(IPolicyContext context)
        {
            this.ctx = context;
        }

        public void AddRisk(string nameOfInsuredObject, Risk risk, DateTime validFrom)
        {
            var policy = ctx.PolicyList.SingleOrDefault(
                p => p.NameOfInsuredObject == nameOfInsuredObject && p.ValidFrom <= validFrom
            );
            if (policy == null) throw new KeyNotFoundException("No valid policies found");

            policy.InsuredRisks.Add(risk);
        }

        public void RemoveRisk(string nameOfInsuredObject, Risk risk, DateTime validTill)
        {
            var policy = ctx.PolicyList.SingleOrDefault(
                p => p.NameOfInsuredObject == nameOfInsuredObject && p.ValidFrom <= validTill
            );
            if (policy == null)
                throw new KeyNotFoundException("No valid policies found");
            if (!policy.InsuredRisks.Contains(risk))
                throw new KeyNotFoundException("Policy does not contain such risk");

            policy.InsuredRisks.Remove(risk);
        }

        public IPolicy SellPolicy(IPolicy policy)
        {
            var existingPolicies = ctx.PolicyList.Where(
                p => (
                    p.ValidFrom >= policy.ValidFrom && p.ValidTill <= policy.ValidTill
                    || p.ValidFrom <= policy.ValidFrom && p.ValidTill >= policy.ValidFrom
                    || p.ValidFrom <= policy.ValidFrom && p.ValidTill >= policy.ValidTill
                ) && p.NameOfInsuredObject == policy.NameOfInsuredObject
            );
            if (existingPolicies.Any())
                throw new InvalidOperationException("A policy already exists within this timeframe");

            ctx.PolicyList.Add(policy);
            return policy;
        }

        public IPolicy GetPolicy(string nameOfInsuredObject, DateTime effectiveDate)
        {
            return ctx.PolicyList.SingleOrDefault(
                p => p.NameOfInsuredObject == nameOfInsuredObject
                    && p.ValidFrom <= effectiveDate
                    && p.ValidTill >= effectiveDate
            );
        }

        public List<IPolicy> List()
        {
            return ctx.PolicyList;
        }

    }
}