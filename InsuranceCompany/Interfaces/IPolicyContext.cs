using System;
using System.Collections.Generic;

namespace Insurance.Interfaces
{
    public interface IPolicyContext
    {
        List<IPolicy> PolicyList { get; }
    }
}