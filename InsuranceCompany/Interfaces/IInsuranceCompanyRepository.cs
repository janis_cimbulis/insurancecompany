using System;
using System.Collections.Generic;
using Insurance.Interfaces;
using Insurance.Structs;

namespace Insurance.Interfaces
{
    public interface IInsuranceCompanyRepository
    {
        void AddRisk(string nameOfInsuredObject, Risk risk, DateTime validFrom);
        void RemoveRisk(string nameOfInsuredObject, Risk risk, DateTime validTill);
        IPolicy SellPolicy(IPolicy policy);
        IPolicy GetPolicy(string nameOfInsuredObject, DateTime effectiveDate);
        List<IPolicy> List();
    }
}