using System;
using System.Collections.Generic;
using System.Linq;
using Insurance.Interfaces;
using Insurance.Models;
using Insurance.Structs;


namespace Insurance
{
    public class InsuranceCompany : IInsuranceCompany
    {

        public InsuranceCompany(string name, IList<Risk> availableRisks, IInsuranceCompanyRepository insuranceCompanyRepository)
        {
            this.Name = name;
            this.AvailableRisks = availableRisks;
            this.icRepository = insuranceCompanyRepository;
        }

        /// <summary>
        /// Insurance Company Repository
        /// </summary>
        private IInsuranceCompanyRepository icRepository { get; set; }

        /// <summary>
        /// Name of Insurance company 
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// List of the risks that can be insured. List can be updated at any time 
        /// </summary>
        public IList<Risk> AvailableRisks { get; set; }

        /// <summary>
        /// Add risk to the policy of insured object.
        /// </summary>
        /// <param name="nameOfInsuredObject">Name of insured object</param>
        /// <param name="risk">Risk that must be added</param>
        /// <param name="validFrom">Date when risk becomes active. Can not be in the past</param>
        /// <summary>
        public void AddRisk(string nameOfInsuredObject, Risk risk, DateTime validFrom)
        {
            if (!AvailableRisks.Contains(risk))
                throw new InvalidOperationException("Risk can not be added to given policy");

            icRepository.AddRisk(nameOfInsuredObject, risk, validFrom);
        }

        /// Remove risk from the policy of insured object.
        /// </summary>
        /// <param name="nameOfInsuredObject">Name of insured object</param>
        /// <param name="risk">Risk that must be removed</param>
        /// <param name="validTill">Date when risk become inactive. Must be equal to or greater than date when risk become active</param>
        /// <summary>
        public void RemoveRisk(string nameOfInsuredObject, Risk risk, DateTime validTill)
        {
            // date when *risk* became inactive ?
            icRepository.RemoveRisk(nameOfInsuredObject, risk, validTill);
        }

        /// <summary>
        /// Sell the policy.
        /// </summary>
        /// <param name="nameOfInsuredObject">Name of the insured object. Must be unique in the given period.</param>
        /// <param name="validFrom">Date and time when policy starts. Can not be in the past</param>
        /// <param name="validMonths">Policy period in months</param>
        /// <param name="selectedRisks">List of risks that must be included in the policy</param>
        /// <returns>Information about policy</returns>
        public IPolicy SellPolicy(string nameOfInsuredObject, DateTime validFrom, short validMonths, IList<Risk> selectedRisks)
        {
            if (String.IsNullOrWhiteSpace(nameOfInsuredObject))
                throw new ArgumentOutOfRangeException("Policy must have a name");

            if (validFrom < DateTime.Now)
                throw new ArgumentOutOfRangeException("Policy can not start in the past");

            if (validMonths < 1)
                throw new ArgumentOutOfRangeException("Policy must have a valid period in months");

            if (selectedRisks == null)
                throw new ArgumentNullException("Selected risks can not be null");

            foreach (var r in selectedRisks)
            {
                if (!AvailableRisks.Contains(r))
                    throw new KeyNotFoundException("Risk is not available in given policy");
            }

            var validTill = validFrom.AddMonths(validMonths);

            var policy = new Policy()
            {
                NameOfInsuredObject = nameOfInsuredObject,
                ValidFrom = validFrom,
                ValidTill = validTill,
                InsuredRisks = selectedRisks
            };

            return icRepository.SellPolicy(policy);
        }

        /// Gets policy with a risks at the given point of time.
        /// </summary>
        /// <param name="nameOfInsuredObject">Name of insured object</param>
        /// <param name="effectiveDate">Point of date and time, when the policy effective</param> 
        /// <returns></returns>
        public IPolicy GetPolicy(string nameOfInsuredObject, DateTime effectiveDate)
        {
            return icRepository.GetPolicy(nameOfInsuredObject, effectiveDate);
        }
    }
}