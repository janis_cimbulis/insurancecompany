using System;

namespace Insurance.Structs
{
    public struct Risk
    {
        /// <summary>
        /// Unique name of the risk
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Risk yearly price
        /// </summary>
        public decimal YearlyPrice { get; set; }

        public override bool Equals(object obj)
        {
            var r = (Risk)obj;
            return this.Name == r.Name && this.YearlyPrice == r.YearlyPrice;
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public override string ToString()
        {
            return $"{Name}-{YearlyPrice}";
        }
    }
}

