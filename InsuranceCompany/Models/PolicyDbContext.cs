using System;
using System.Collections.Generic;
using Insurance.Interfaces;

namespace Insurance.Models
{
    public class PolicyDbContext : IPolicyContext
    {
        public List<IPolicy> PolicyList { get; }

        public PolicyDbContext(List<IPolicy> policyList)
        {
            this.PolicyList = policyList;
        }

    }
}