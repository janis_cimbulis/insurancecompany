using System;
using System.Collections.Generic;
using System.Linq;
using Insurance.Interfaces;
using Insurance.Structs;

namespace Insurance.Models
{
    public class Policy : IPolicy
    {
        /// <summary>
        /// Name of insured object
        /// </summary>
        public string NameOfInsuredObject { get; set; }

        /// <summary>
        /// Date when policy becomes active 
        /// </summary>
        public DateTime ValidFrom { get; set; }

        /// <summary>
        /// Date when policy becomes inactive 
        /// </summary>
        public DateTime ValidTill { get; set; }

        /// <summary>
        /// Total price of the policy. Calculate by summing up all insured risks.
        /// Take into account that risk price is given for 1 full year. Policy/risk period can be shorter. 
        /// </summary>
        public decimal Premium
        {
            get
            {
                var yearStart = new DateTime(DateTime.Now.Year, 1, 1);
                var yearEnd = new DateTime(DateTime.Now.Year, 12, 31);
                var daysInYear = (yearEnd - yearStart).TotalDays;
                var policyDuration = ValidTill - ValidFrom;
                decimal totalPrice = 0;

                foreach (var r in InsuredRisks)
                {
                    totalPrice += (r.YearlyPrice / (decimal)daysInYear) * (decimal)policyDuration.TotalDays;
                }
                return totalPrice;
            }
        }

        /// <summary>
        /// Initially included risks or risks at specific moment of time. 
        /// </summary>
        public IList<Risk> InsuredRisks { get; set; }


        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            Policy p = obj as Policy;
            
            if (
                this.NameOfInsuredObject != p.NameOfInsuredObject
                || this.ValidFrom != p.ValidFrom
                || this.ValidTill != p.ValidTill
                || this.Premium != p.Premium
            ) return false;

            if (    
                !Enumerable.SequenceEqual(
                    this.InsuredRisks.OrderBy(r => r.Name),
                    p.InsuredRisks.OrderBy(r => r.Name)
                )
            ) return false;

            return true;
        }

        public override int GetHashCode()
        {
            var hash = $"{NameOfInsuredObject}-{ValidFrom}-{ValidTill}";
            foreach (var r in InsuredRisks)
            {
                hash += $"-{r.ToString()}";
            }
            return hash.GetHashCode();
        }
    }
}