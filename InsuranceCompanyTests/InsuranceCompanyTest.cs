using System;
using Xunit;
using System.Collections.Generic;
using Insurance;
using Insurance.Interfaces;
using Insurance.Infrastructure;
using Insurance.Models;
using Insurance.Structs;

namespace InsuranceCompanyTests
{
    public class InsuranceCompanyTest
    {
        InsuranceCompanyRepository icRepo;
        InsuranceCompany icInstance;
        Policy testAgainst;
        Risk CatRisk;
        Risk FoodRisk;
        Risk LaptopRisk;
        Risk TableRisk;
        Risk CoffeeRisk;

        public InsuranceCompanyTest()
        {
            CatRisk = new Risk() { Name = "Cat", YearlyPrice = 1000 };
            FoodRisk = new Risk() { Name = "Fridge and Food", YearlyPrice = 5000 };
            LaptopRisk = new Risk() { Name = "Laptop", YearlyPrice = 500 };
            TableRisk = new Risk() { Name = "Table", YearlyPrice = 20 };
            CoffeeRisk = new Risk() { Name = "Coffee", YearlyPrice = 100 };

            testAgainst = new Policy()
            {
                NameOfInsuredObject = "John C Apartment",
                ValidFrom = new DateTime(2018, 7, 1),
                ValidTill = new DateTime(2018, 12, 31),
                InsuredRisks = new List<Risk>() {
                    CatRisk,
                    FoodRisk
                }
            };

            var ctx = new PolicyDbContext(new List<IPolicy>()
            {
                testAgainst
            });

            icRepo = new InsuranceCompanyRepository(ctx);

            icInstance = new InsuranceCompany(
                "Insurance",
                new List<Risk>(){
                    CatRisk,
                    FoodRisk,
                    LaptopRisk,
                    TableRisk
                },
                icRepo
            );
        }

        [Fact]
        public void GetPolicy_ShouldReturnExistingPolicy()
        {
            var existingPolicy = icInstance.GetPolicy(
                "John C Apartment",
                new DateTime(2018, 7, 1)
            );
            Assert.Equal(testAgainst, existingPolicy);
        }

        [Fact]
        public void GetPolicy_ShouldBeNull()
        {
            var doesntExist = icInstance.GetPolicy("John B Apartment", new DateTime(2017, 7, 1));
            Assert.True(doesntExist == null);
        }

        [Fact]
        public void SellPolicy_ShouldAddNewPolicy()
        {
            var nameOfInsuredObject = "John C Apartment";
            var validFrom = new DateTime(2018, 1, 1);
            short validMonths = 3;
            var insuredRisks = new List<Risk>() {
                LaptopRisk
            };

            icInstance.SellPolicy(nameOfInsuredObject, validFrom, validMonths, insuredRisks);
            var hasPolicy = icInstance.GetPolicy(nameOfInsuredObject, validFrom);
            Assert.True(hasPolicy.GetType() != null);
            Assert.True(hasPolicy.InsuredRisks.Count == 1);
        }

        [Fact]
        public void SellPolicy_ShouldNotAdd_NoSuchRisk()
        {
            var nameOfInsuredObject = "John C Apartment";
            var validFrom = new DateTime(2018, 1, 1);
            short validMonths = 3;
            // Coffee risk is not in the list of insured risks
            var insuredRisks = new List<Risk>() {
                CoffeeRisk,
                LaptopRisk
            };

            Assert.Throws<KeyNotFoundException>(
                () => icInstance.SellPolicy(nameOfInsuredObject, validFrom, validMonths, insuredRisks)
            );
        }
        [Fact]
        public void SellPolicy_ShouldNotAdd_ArgumentNull()
        {
            var validFrom = new DateTime(2018, 1, 1);
            short validMonths = 3;
            var insuredRisks = new List<Risk>() {
                LaptopRisk
            };

            Assert.Throws<ArgumentOutOfRangeException>(
                () => icInstance.SellPolicy(null, validFrom, validMonths, insuredRisks)
            );
        }

        [Fact]
        public void SellPolicy_ShouldNotAdd_Exists()
        {
            Assert.Throws<InvalidOperationException>(
                () => icInstance.SellPolicy(
                    testAgainst.NameOfInsuredObject,
                    testAgainst.ValidFrom,
                    3,
                    testAgainst.InsuredRisks
                )
            );
        }


        [Fact]
        public void AddRisk_ShouldAdd()
        {
            var p = icInstance.GetPolicy(
                testAgainst.NameOfInsuredObject,
                testAgainst.ValidFrom
            );

            Assert.True(p.InsuredRisks.Count == 2);

            icInstance.AddRisk(
                testAgainst.NameOfInsuredObject,
                TableRisk,
                testAgainst.ValidFrom.AddDays(1)
            );

            var p1 = icInstance.GetPolicy(
                testAgainst.NameOfInsuredObject,
                testAgainst.ValidFrom
            );

            Assert.True(p1.InsuredRisks.Count == 3);
        }

        [Fact]
        public void AddRisk_ShouldNotAddUnavailable()
        {
            Assert.Throws<InvalidOperationException>(
                () => icInstance.AddRisk(
                testAgainst.NameOfInsuredObject,
                CoffeeRisk,
                testAgainst.ValidFrom
            ));
        }

        [Fact]
        public void RemoveRisk_ShouldRemove()
        {
            var p = icInstance.GetPolicy(
                testAgainst.NameOfInsuredObject,
                testAgainst.ValidFrom
            );

            Assert.True(p.InsuredRisks.Count == 2);

            icInstance.RemoveRisk(
                testAgainst.NameOfInsuredObject,
                CatRisk,
                testAgainst.ValidTill
            );

            var p1 = icInstance.GetPolicy(
                testAgainst.NameOfInsuredObject,
                testAgainst.ValidFrom
            );

            Assert.True(p1.InsuredRisks.Count == 1);

        }

        [Fact]
        public void RemoveRisk_ShouldNotRemove_NoPolicies()
        {
            Assert.Throws<KeyNotFoundException>(
                () => icInstance.RemoveRisk(
                    testAgainst.NameOfInsuredObject,
                    CoffeeRisk,
                    testAgainst.ValidFrom
                ));
        }

        [Fact]
        public void RemoveRisk_ShouldNotRemove_NoSuchRisk()
        {
            Assert.Throws<KeyNotFoundException>(
                () => icInstance.RemoveRisk(
                    $"{testAgainst.NameOfInsuredObject}-111",
                    CoffeeRisk,
                    testAgainst.ValidFrom
                ));
        }

    }
}
